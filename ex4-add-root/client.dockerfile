FROM mongo:4
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
COPY wait-then-run.sh /wait-then-run.sh
RUN chmod +x /wait /wait-then-run.sh
ENTRYPOINT [ "/wait-then-run.sh" ]
