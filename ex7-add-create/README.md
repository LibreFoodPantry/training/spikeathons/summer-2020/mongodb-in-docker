# ex7-add-create

We've established some basic security for our database client and server.
It's time to start dealing with data.

In the area of databases, there are four operations for manipulating data in a
database: create, read, update, and delete. Collectively they are referred to
as CRUD operations.

In this example, we start evaluating MongoDB JavaScript-based Shell scripts
and introduce our first "create" operation. Without a "create" operation we
would have nothing to read, update, or delete.

```bash
cd ex7-add-create
```

Examine `create.js`. This is a MongoDB JavaScript file that will create a new
cats collection (if it does not already exist), and three cats. Also not the
authentication.

Examine `client.dockerfile` and notice how `create.js` is copied into the root
of the container. It could have been copied anywhere. We chose the root for
convenience.

Examine `docker-compose.yml` and notice how `create.js` is passed to `mongo`
in the client's command. Notice also that there is no authentication in the
command to mongo. This has moved into `create.js`. And the password is being
passed as a files by mounting `secrets` into the client. So you will not be
prompted for a password as in previous examples.

As with ex5-add-non-root, we need to create a secrets directory containing
our passwords that will be mounted into the server's container. You could
copy this from the previous example (if you did not delete it) or reproduce it
here.

```bash
mkdir -p secrets
echo "some-password" > secrets/mongo-initdb-root-password.txt
echo "another-password" > secrets/test-owner-password.txt
```

Generate the TLS files. (This too could be copied from the previous example
if you didn't delete it.)

```bash
TLS_HOME="${PWD}/tls" docker-compose -f gen-tls.yml run --rm gen-tls client server
```

Run our server and client.

```bash
docker-compose up --detach
docker-compose logs
```

Examine the output and notice the log of the server indicates that cats are
being added.

Time to clean up.

```bash
docker-compose down
rm -r tls secrets
cd ..
```
